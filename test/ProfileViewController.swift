//
//  ProfileViewController.swift
//  test
//
//  Created by Дмитрий on 15.03.2018.
//  Copyright © 2018 Dmytro Zabolotnyi. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON


class   ProfileViewController: UIViewController {
    
    var json: JSON?
    
    @IBOutlet weak var jsonTest: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tryPrint()
        //refreshUI()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        jsonTest.text = ""
        
    }
    
    func tryPrint() {
        jsonTest.text = json!["displayname"].string;
    }
}
