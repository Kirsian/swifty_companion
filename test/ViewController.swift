//
//  ViewController.swift
//  test
//
//  Created by Dmytro Zabolotnyi on 2/16/18.
//  Copyright © 2018 Dmytro Zabolotnyi. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON


class ViewController: UIViewController {

    @IBOutlet weak var Find: UIButton!
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var coffee: UIImageView!
    @IBOutlet weak var donuts: UIImageView!
    @IBOutlet weak var buttonRect: UIView!
    @IBOutlet weak var police: UIImageView!
    
    var appAuth = AppInit()
    var jsonRes: JSON?
    var json: JSON?
    var userProfile: Profile?
    
    
    func refreshUI() {
        login.text = ""
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Dzabolot Unit Police"
        self.view.backgroundColor = UIColorFromRGB(rgbValue: 0x18212d)
        self.buttonRect.layer.cornerRadius = 15.0
        refreshUI()
        appAuth.checkToken()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  

    @IBAction func viewTapped(sender : AnyObject) {
        
    }
    
    
    func kostil(json: JSON) {
        //resultTextView.text = ("\(json["projects_users"][2]["final_mark"].int!)");
        print(json["projects_users"][2]["final_mark"])
        //performSegue(withIdentifier: "profileSegue", sender: json)
        //print(json)
    }
    
    func userAlert(title: String, body: String, button: String) {
        let alert = UIAlertController( title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    
    @IBAction func findInfo(sender: AnyObject) {
        let _login = login.text!
        
        if (_login.count != 8) {
            userAlert(title: "Error", body: "Login is incorrect >_<", button: ":(")
        }
        else {
            appAuth.checkUser(login: _login){
                completion in
                if completion != nil {
                    self.jsonRes = completion
                    self.kostil(json: self.jsonRes!)
                    self.userProfile = self.appAuth.fillUserProfile(result: self.jsonRes!)
                    self.performSegue(withIdentifier: "profileSegue", sender: self)
                    
                } else {
                    let alert = UIAlertController(title: "error", message: "Login doesn't exist", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileSegue" {
            if let vc = segue.destination as? StudentInfoTableViewController {
                print("kek \(String(describing: userProfile?.name))")
                vc.profile = userProfile!
            }
        }
    }
    
}

