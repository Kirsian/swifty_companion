//
//  ProfileCell.swift
//  Alamofire
//
//  Created by Дмитрий on 23.03.2018.
//

import Foundation
import UIKit


class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var level: UILabel!
    @IBOutlet weak var levelProgresBar: UIProgressView!
    @IBOutlet weak var wallet: UILabel!
    @IBOutlet weak var correctionPoints: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var avaliable: UILabel!
    @IBOutlet weak var mail: UILabel!
    @IBOutlet weak var fname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func fillInformation(for profile: Profile) {
        if let name = profile.name {
                fname.text = name
        }
        if let loginVar = profile.login {
            login.text = loginVar
        }

        if let path = profile.pic {
            if let url = URL(string: path) {
                if let data = try? Data(contentsOf: url) {
                    ProfilePic.layer.masksToBounds = true
                    ProfilePic.layer.cornerRadius = 50.0
                    ProfilePic.image = UIImage(data: data)
                }
            }
        }
        if let lvl = profile.level {
            level.text = "Level: \(String(format: "%.2f", lvl))%"
        }
        if let wlt = profile.wallet {
            wallet.text = "Wallet: \(String(wlt)) 💴"
        }
        if let cp = profile.corrPoint {
            correctionPoints.text = "Correction Points: \(String(cp))"
        }
        if let phn = profile.mobile {
            phone.text = "\(phn)"
        }
        if let email = profile.mail {
            mail.text = "\(email)"
            mail.minimumScaleFactor = 0.5
        }
        if let location = profile.location {
            avaliable.text = "location: \(location)"
        } else {
            avaliable.text = "unavailable"
        }
        
        fillProgressBar(for: levelProgresBar, with: profile.level!)
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func fillProgressBar(for progressBar: UIProgressView, with level: Float) {
        let progr = level.truncatingRemainder(dividingBy: 1)
        progressBar.progress = progr
        progressBar.clipsToBounds = true
        progressBar.progressTintColor = UIColorFromRGB(rgbValue: 0xFBBA00)
        progressBar.trackTintColor = UIColorFromRGB(rgbValue: 0xf2f2f2)
        progressBar.layer.borderWidth = 0.1
        progressBar.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    
}
