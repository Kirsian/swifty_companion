//
//  ProjectTableViewCell.swift
//  Pods
//
//  Created by Дмитрий on 23.03.2018.
//

import Foundation
import UIKit

class ProjectTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var backg: UIView!
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var projectMark: UILabel!
    @IBOutlet weak var backForMark: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backg.layer.cornerRadius = 8.0
        backForMark.layer.cornerRadius = 10.0
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func fillProject(with project: Projects) {
        projectName.text = project.title
        if (project.success! && project.mark != 0) {
            projectMark.text = String(describing: project.mark!)
            projectMark.textColor = UIColorFromRGB(rgbValue: 0x66ff66)
        } else {
            projectMark.text = String(describing: project.mark!)
            projectMark.textColor = UIColorFromRGB(rgbValue: 0xff3300)
        }
    }
}
