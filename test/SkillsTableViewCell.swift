//
//  SkillsTableViewCell.swift
//  Pods
//
//  Created by Дмитрий on 23.03.2018.
//

import Foundation
import UIKit

class SkillsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fillSkils(with skill: Skills) {
        name.text = skill.title! + " level: \(String(format: "%.2f", skill.level!))"
        fillProgressBar(for: progressBar, with: skill.level!)
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func fillProgressBar(for progressBar: UIProgressView, with level: Float) {
        let progr = level.truncatingRemainder(dividingBy: 1)
        progressBar.progress = progr
        progressBar.clipsToBounds = true
        progressBar.progressTintColor = UIColorFromRGB(rgbValue: 0xFBBA00)
        progressBar.trackTintColor = UIColorFromRGB(rgbValue: 0xf2f2f2)
        progressBar.layer.borderWidth = 0.2
        progressBar.layer.borderColor = UIColor.lightGray.cgColor
    }
    
}
