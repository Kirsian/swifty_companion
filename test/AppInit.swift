//
//  AppInit.swift
//  test
//
//  Created by Дмитрий on 22.02.2018.
//  Copyright © 2018 Dmytro Zabolotnyi. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class AppInit: NSObject {
    var token = String()
    var str = String()
    let url = "https://api.intra.42.fr/oauth/token"
    let config = [
        "grant_type": "client_credentials",
        "client_id": "24684cd5a5c06baf6b095e86c1dfd96b684bd169331ffa370228c35e2d6349bc",
        "client_secret": "4242cf8e475b8b568e5a2efbe050c33955748112c3710059a41254cf6b7f1308"
    ]
    let defaults = UserDefaults.standard
    
    
    func getToken() {
        let verify = defaults.string(forKey: "Token")
        if verify == nil {
            Alamofire.request(url, method: .post, parameters: config).validate().responseJSON {
                response in
                switch response.result {
                case .success:
                    if let val = response.result.value {
                        let json = JSON(val)
                        self.token = json["access_token"].stringValue
                        self.defaults.set(self.token, forKey: "Token")
                        print(self.defaults.string(forKey: "Token"))
                        print("You have a new token: ", self.token)
                        self.checkToken()
                    }
                case .failure(_):
                    print("Error message:\(response.result.error)\nProblem with geting new token")
                    
                }
            }
        } else {
            self.token = verify as! String
            print("Token stil valid: \(self.token)");
            checkToken()
        }
    }
    
    func checkToken() {
        let infoUrl = URL(string: "https://api.intra.42.fr/oauth/token/info")
        let tok = "Bearer " + self.token
        print("Authorization: \(tok)")
        let heads: HTTPHeaders = [
            "Authorization": tok
        ]
        Alamofire.request(infoUrl!, method: .get, headers: heads).validate().responseJSON {
            response in
            switch response.result {
            case .success(_):
                if let val = response.result.value {
                    let json = JSON(val)
                    print("Your token will expire in \(json["expires_in_seconds"]) s.")
                }
            case .failure(_):
                print("Yout token was expire :(\nlets get the new one \(response.result.error)")
                self.defaults.removeObject(forKey: "Token")
                self.getToken()
            }
        }
    }
    
    func checkUser(login: String, completion: @escaping (JSON?) -> Void) {
        checkToken()
        defer { let chooseUser = "https://api.intra.42.fr/v2/users/" + login;
            let tok = "Bearer " + self.defaults.string(forKey: "Token")!
            print("Authorization: \(tok)")
            let heads: HTTPHeaders = [
                "Authorization": tok
            ]
            Alamofire.request(chooseUser, method: .get, headers: heads).validate().responseJSON {
                response in
                switch response.result {
                case .success(_):
                    if let val = response.result.value {
                        let json = JSON(val)
                        completion(json)
                        print("done")
                    }
                case .failure(_):
                    completion(nil)
                    print(login)
                    print("Error, this login isn't correct! Error is\n\(String(describing: response.result.error))")
                }
            }
        }
    }
    
    func fillUserProfile(result: JSON) -> Profile {
        var userProfile = Profile()
        
        if let log = result["login"].string {
            userProfile.login = log
        }
        if let location = result["location"].string {
            userProfile.location = location
        }
        if let fullname = result["displayname"].string {
            userProfile.name = fullname
        }
        if let img = result["image_url"].string {
            userProfile.pic = img
        }
        if let cp = result["correction_point"].int {
            userProfile.corrPoint = cp
        }
        if let email = result["email"].string {
            userProfile.mail = email
        }
        if let phone = result["phone"].string {
            if (phone.isEmpty) {
                userProfile.mobile = "empty("
            } else {
                userProfile.mobile = phone
            }
        }
        if let wl = result["wallet"].int {
            userProfile.wallet = wl
        }
        if let lvl = result["cursus_users"][0]["level"].float {
            userProfile.level = lvl
        }
        for (key, subJson) in result["cursus_users"][0]["skills"] {
            userProfile.skills.append(Skills(title: subJson["name"].string, level: subJson["level"].float!))
        }
        for (_, subJson) in result["projects_users"] {
            if (subJson["final_mark"] != nil && subJson["validated?"] != nil && subJson["project"]["parent_id"] != 167) {
                let ttl =  subJson["project"]["name"].string!
                let mrk =  subJson["final_mark"].int!
                let val = subJson["validated?"].bool!
                userProfile.projects.append(Projects(mark: mrk, title: ttl, success: val))
            }
        }
        return userProfile
    }
}

