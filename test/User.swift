//
//  User.swift
//  Alamofire
//
//  Created by Дмитрий on 22.03.2018.
//

import Foundation

struct Profile {
    var pic: String?
    var name: String?
    var surname: String?
    var login: String?
    var mobile: String?
    var mail: String?
    var wallet: Int?
    var corrPoint: Int?
    var location: String?
    var level: Float?
    var skills = [Skills]()
    var projects = [Projects]()
}

struct Skills {
    var title: String?
    var level: Float?
}

struct Projects {
    var mark: Int?
    var title: String?
    var success: Bool?
}

